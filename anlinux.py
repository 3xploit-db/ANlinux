#!/usr/local/bin/python
# code by WongNdeso // https://github.com/3xploit-db/
# THANKS for EXALAP // https://github.com/EXALAB




import os
# set warna
R = '\033[31m'   # red
N = '\033[1;37m' # white
G = '\033[32m'   # green
O = '\033[0;33m' # Orange
B = '\033[1;34m' # Blue    

def menu():
#########################################################################################################################################
 print""+G+"  _    _   _ _ _"                  
 print"      / \  | \ | | (_)_ __  _   ___  __"
 print"     / _ \ |  \| | | | '_ \| | | \ \/ /"
 print"    / ___ \| |\  | | | | | | |_| |>  < "
 print"   /_/   \_\_| \_|_|_|_| |_|\__,_/_/\_\ " 
 print"                      Termux Installer "
 print""                            
 print""+R+"[+] Coded By 3xploit //https://github.com/3xploit-db"
 print"[+] Thanks for Anlinux //https://github.com/EXALAB "
 print""+B+"========================================"
 print" 1. Ubuntu "
 print" 2. Debian "
 print" 3. kali linux "
 print" 4. kali Nethunter "
 print" 5. Parrot Security OS "
 print" 6. Fedora "
 print" 7. CentOS "
 print" 8. openSUSE Leap "
 print" 9. openSUSE Tumbleweed "
 print" 10. Arch Linux "
 print" 11. black arch "
 print" 12. Alpine "
 print"-----------------------------------------"
 print" 0. Exit "
 print"=========================================="
 print""+G+""
xploit = True
##########################################################################################################################################
while xploit:
    menu()
    xploit = raw_input("[+] ~Input: ")
    if xploit == "1":
        os.system("cd /data/data/com.termux/files/home")
        os.system("pkg update -y")
        os.system("pkg install wget proot -y && wget https://raw.githubusercontent.com/EXALAB/AnLinux-Resources/master/Scripts/Installer/Ubuntu/ubuntu.sh && bash ubuntu.sh ")
        os.system("cd /data/data/com.termux/files/home")
        print("================================================")
        print("[+] Ubuntu Success Install")
        print("[+] Untuk menjalankan ketik $ ./start-ubuntu.sh")
        print("================================================")
        menu = input("[?] Back to Menu? (y/n): ")
        if menu == "y":
            menu()
        else:
            break
    elif xploit == "2":
        os.system("cd /data/data/com.termux/files/home")
        os.system("pkg update -y")
        os.system("pkg install wget proot -y && wget https://raw.githubusercontent.com/EXALAB/AnLinux-Resources/master/Scripts/Installer/Debian/debian.sh && bash debian.sh")
        os.system("cd /data/data/com.termux/files/home")
        print("================================================")
        print("[+] Debian success Install")
        print("[+] Untuk menjalankan ketik $ ./start-debian.sh ")
        print("================================================")
        menu = input("[?] Back to Menu? (y/n): ")
        if menu == "y":
            menu()
        else:
            break
    elif xploit == "3":
        os.system("cd /data/data/com.termux/files/home")
        os.system("pkg update -y")
        os.system("pkg install wget proot -y && wget https://raw.githubusercontent.com/EXALAB/AnLinux-Resources/master/Scripts/Installer/Kali/kali.sh && bash kali.sh")
        os.system("cd /data/data/com.termux/files/home")
        print("================================================")
        print("[+] Kali linux success Install")
        print("[+] Untuk menjalankan ketik $ ./start-kali.sh ")
        print("================================================")
        menu = input("[?] Back to Menu? (y/n): ")
        if menu == "y":
            menu()
        else:
            break
    elif xploit == "4":
        os.system("cd /data/data/com.termux/files/home")
        os.system("pkg update -y")
        os.system("pkg install wget proot -y && wget https://raw.githubusercontent.com/EXALAB/AnLinux-Resources/master/Scripts/Installer/Nethunter/nethunter.sh && bash nethunter.sh")
        os.system("cd /data/data/com.termux/files/home")
        print("================================================")
        print("[+] Kali nethunter success Install")
        print("[+] Untuk menjalankan ketik $ ./start-nethunter.sh ")
        print("================================================")
        menu = input("[?] Back to Menu? (y/n): ")
        if menu == "y":
            menu()
        else:
            break
    elif xploit == "5":
        os.system("cd /data/data/com.termux/files/home")
        os.system("pkg update -y")
        os.system("pkg install wget proot -y && wget https://raw.githubusercontent.com/EXALAB/AnLinux-Resources/master/Scripts/Installer/Parrot/parrot.sh && bash parrot.sh")
        os.system("cd /data/data/com.termux/files/home")
        print("================================================")
        print("[+] parrot security os success Install")
        print("[+] Untuk menjalankan ketik $ ./start-parrot.sh ")
        print("================================================")
        menu = input("[?] Back to Menu? (y/n): ")
        if menu == "y":
            menu()
        else:
            break
    elif xploit == "6":
        os.system("cd /data/data/com.termux/files/home")
        os.system("pkg update -y")
        os.system("pkg install wget proot tar -y && wget https://raw.githubusercontent.com/EXALAB/AnLinux-Resources/master/Scripts/Installer/Fedora/fedora.sh && bash fedora.sh")
        os.system("cd /data/data/com.termux/files/home")
        print("================================================")
        print("[+] fedora success Install")
        print("[+] Untuk menjalankan ketik $ ./start-fedora.sh ")
        print("================================================")
        menu = input("[?] Back to Menu? (y/n): ")
        if menu == "y":
            menu()
        else:
            break
    elif xploit == "7":
        os.system("cd /data/data/com.termux/files/home")
        os.system("pkg update -y")
        os.system("pkg install wget proot tar -y && wget https://raw.githubusercontent.com/EXALAB/AnLinux-Resources/master/Scripts/Installer/CentOS/centos.sh && bash centos.sh")
        os.system("cd /data/data/com.termux/files/home")
        print("================================================")
        print("[+] centOS success Install")
        print("[+] Untuk menjalankan ketik $ ./start-centos.sh ")
        print("================================================")
        menu = input("[?] Back to Menu? (y/n): ")
        if menu == "y":
            menu()
        else:
            break
    elif xploit == "8":
        os.system("cd /data/data/com.termux/files/home")
        os.system("pkg update -y")
        os.system("pkg install wget proot tar -y && wget https://raw.githubusercontent.com/EXALAB/AnLinux-Resources/master/Scripts/Installer/openSUSE/Leap/opensuse-leap.sh && bash opensuse-leap.sh")
        os.system("cd /data/data/com.termux/files/home")
        print("================================================")
        print("[+] opensuse Leap success Install")
        print("[+] Untuk menjalankan ketik $ ./start-opensuse.sh ")
        print("================================================")
        menu = input("[?] Back to Menu? (y/n): ")
        if menu == "y":
            menu()
        else:
            break
    elif xploit == "9":
        os.system("cd /data/data/com.termux/files/home")
        os.system("pkg update -y")
        os.system("pkg install wget proot tar -y && wget https://raw.githubusercontent.com/EXALAB/AnLinux-Resources/master/Scripts/Installer/openSUSE/Tumbleweed/opensuse-tumbleweed.sh && bash opensuse-tumbleweed.sh")
        print("================================================")
        print("[+] opensuse-tumbleweed success Install")
        print("[+] Untuk menjalankan ketik $ ./start-opensuse.sh ")
        print("================================================")
        menu = input("[?] Back to Menu? (y/n): ")
        if menu == "y":
            menu()
        else:
            break 
    elif xploit == "10":
        os.system("cd /data/data/com.termux/files/home")
        os.system("pkg update -y")
        os.system("pkg install wget proot tar -y && wget https://raw.githubusercontent.com/EXALAB/AnLinux-Resources/master/Scripts/Installer/openSUSE/Tumbleweed/opensuse-tumbleweed.sh && bash opensuse-tumbleweed.sh")
        print("================================================")
        print("[+] opensuse-tumbleweed success Install")
        print("[+] Untuk menjalankan ketik $ ./start-opensuse-tumbleweed.sh ")
        print("================================================")
        menu = input("[?] Back to Menu? (y/n): ")
        if menu == "y":
            menu()
        else:
            break   
    elif xploit == "11":
        os.system("cd /data/data/com.termux/files/home")
        os.system("pkg update -y")
        os.system("pkg install wget proot tar -y && wget https://raw.githubusercontent.com/EXALAB/AnLinux-Resources/master/Scripts/Installer/Arch/armhf/arch.sh && bash arch.sh")
        os.system("cd /data/data/com.termux/files/home")
        print("================================================")
        print("[+] arch success Install")
        print("[+] Untuk menjalankan ketik $ ./start-arch.sh ")
        print("================================================")
        menu = input("[?] Back to Menu? (y/n): ")
        if menu == "y":
            menu()
        else:
            break
    elif xploit == "12":
        os.system("cd /data/data/com.termux/files/home")
        os.system("pkg update -y")
        os.system("pacman-key --init && pacman-key --populate archlinuxarm && pacman -Sy --noconfirm curl && curl -O https://blackarch.org/strap.sh && chmod +x strap.sh && ./strap.sh")
        os.system("cd /data/data/com.termux/files/home")
        print("================================================")
        print("[+] blackarch success Install")
        print("[+] Untuk menjalankan ketik $ ./start-blackarch.sh ")
        print("================================================")
        menu = input("[?] Back to Menu? (y/n): ")
        if menu == "y":
            menu()
        else:
            break
    elif xploit == "13":
        os.system("cd /data/data/com.termux/files/home")
        os.system("pkg update -y")
        os.system("pkg install wget proot tar -y && wget https://raw.githubusercontent.com/EXALAB/AnLinux-Resources/master/Scripts/Installer/Alpine/alpine.sh && bash alpine.sh")
        os.system("cd /data/data/com.termux/files/home")
        print("================================================")
        print("[+] alpine success Install")
        print("[+] Untuk menjalankan ketik $ ./start-alpine.sh ")
        print("================================================")
        menu = input("[?] Back to Menu? (y/n): ")
        if menu == "y":
            menu()
        else:
            break
    elif xploit == "0":	
        print("THANKS Bro!!..")
        break

        # END 
        # 
